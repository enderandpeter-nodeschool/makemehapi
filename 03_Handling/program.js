const Hapi = require('@hapi/hapi');
const Path = require('path');

const init = async () => {
    const server = Hapi.Server({
        host: 'localhost',
        port: Number(process.argv[2] || 8080),
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    })
    
    await server.register(require('@hapi/inert'));

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true
            }
        }
    });

    await server.start();
    console.log('Server running at:', server.info.uri);
}

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();