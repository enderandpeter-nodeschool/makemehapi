const Hapi = require("@hapi/hapi");

const init = async () => {
  const server = Hapi.Server({
    host: "localhost",
    port: Number(process.argv[2] || 8080),
  });

  server.state('session', {
    path: '/',
    encoding: 'base64json',
    ttl: 10,
    domain: 'localhost',
    isSecure: false,
    isHttpOnly: false,
    isSameSite: false,
  })

  server.route({
    method: "GET",
    path: "/set-cookie",
    handler: (request, h) => {
       return h.response("But you're not a man").state('session', { key: 'makemehapi' });
    },
    options: {
        state: {
            parse: true,
            failAction: 'log'
        }
    }
  });

  server.route({
    method: "GET",
    path: "/check-cookie",
    handler: (request, h) => {
       if(request.state.session){
           return {user: 'hapi'}
       } else {
           return h.response('unauthorized').code(401)
       }
    },
    options: {
        state: {
            parse: true,
            failAction: 'log'
        }
    }
  });

  await server.start();
  console.log("Server running at:", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
