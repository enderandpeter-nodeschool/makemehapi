const Hapi = require('@hapi/hapi');
const Vision = require('@hapi/vision');
const Handlebars = require('handlebars');

const init = async () => {
    const server = Hapi.Server({
        host: 'localhost',
        port: Number(process.argv[2] || 8080)
    })
    
    await server.register(Vision);

    server.views({
        engines: {
            html: Handlebars
        },
        relativeTo: __dirname,
        path: 'templates',
        helpersPath: 'templates/helpers'
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: {
            view: "index.html"
        }
    });

    await server.start();
    console.log('Server running at:', server.info.uri);
}

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();