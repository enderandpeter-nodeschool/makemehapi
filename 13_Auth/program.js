const Hapi = require("@hapi/hapi");
const HttpBasicAuth = require("@hapi/basic");

const validate = async (request, username, password) => {
    const credentials = {
        username
    }
    let isValid = false;

    if(username === 'hapi' && password === 'auth'){
        isValid = true;
    }

    return { credentials, isValid }
}

const init = async () => {
  const server = Hapi.Server({
    host: "localhost",
    port: Number(process.argv[2] || 8080),
  });

  await server.register(HttpBasicAuth);

  server.auth.strategy('simple', 'basic', { validate });
  server.auth.default('simple');

  server.route({
    method: "GET",
    path: "/",
    handler: (request, h) => {
       return 'welcome';
    }
  });

  await server.start();
  console.log("Server running at:", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
