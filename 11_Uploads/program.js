const Hapi = require("@hapi/hapi");
const Joi = require("joi");

const init = async () => {
  const server = Hapi.Server({
    host: "localhost",
    port: Number(process.argv[2] || 8080),
  });

  server.route({
    method: "POST",
    path: "/upload",
    handler: (request, h) => new Promise((resolve, reject) => {
        let body = '';
        request.payload.file.on('data', function (data){

          body += data
        });

        request.payload.file.on('end', function (){
          const { description }  = request.payload;
          const { filename, headers } = request.payload.file.hapi
          const responseData = JSON.stringify({
                description,
                file: {
                    data: body,
                    filename,
                    headers
                }
            });

            return resolve(responseData)
        });
        
        request.payload.file.on('error', err => reject(err));
    }),
    options: {
        payload: {
            output : 'stream',
            parse : true,
            multipart: true
        }
    }
  });

  await server.start();
  console.log("Server running at:", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
