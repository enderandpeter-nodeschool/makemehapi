const Hapi = require("@hapi/hapi");
const Joi = require("joi");

const init = async () => {
  const server = Hapi.Server({
    host: "localhost",
    port: Number(process.argv[2] || 8080),
  });

  server.route({
    method: "POST",
    path: "/login",
    handler: (request, h) => {
       return h.response('login successful');
    },
    options: {
        validate: {
            payload: Joi.object({
                 username: Joi.string().when('isGuest', {
                     is: false,
                     then: Joi.required()
                 }),
                 isGuest: Joi.boolean(),
                 password: Joi.string().alphanum(),
                 accessToken: Joi.string().alphanum()
            })
            .options({allowUnknown: true})
            .without('password', 'accessToken')
         }
    }
  });

  await server.start();
  console.log("Server running at:", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
