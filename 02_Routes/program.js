const Hapi = require('@hapi/hapi');
const Hoek = require('@hapi/hoek');

const init = async () => {
    const server = Hapi.Server({
        host: 'localhost',
        port: Number(process.argv[2] || 8080)
    })
    
    server.route({path: '/{name}', method:'GET', handler: (request, h) => `Hello ${Hoek.escapeHtml(request.params.name)}`});

    await server.start();
    console.log('Server running at:', server.info.uri);
}

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();